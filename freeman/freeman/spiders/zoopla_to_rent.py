# -*- coding: utf-8 -*-
import scrapy
import re
from decimal import Decimal
from datetime import datetime
from freeman.items import Zoopla


class ZooplaToRentSpider(scrapy.Spider):
    name = 'zoopla_to_rent'
    allowed_domains = ['zoopla.co.uk']
    # start_urls = ['https://www.zoopla.co.uk/to-rent/houses/e15/?q=e15&radius=0&property_sub_type=&beds_min=&beds_max=&price_max=&price_min=&page_size=10',
    # 'https://www.zoopla.co.uk/to-rent/houses/e15/?q=e15&radius=0&property_sub_type=&beds_min=&beds_max=&price_max=&price_min=&page_size=10']
    def __init__(self, property_type='houses', location='e14,e15', radius=0,property_sub_type='',beds_min='',beds_max='',price_max='',price_min='',page_size=100, **kwargs):
    # def __init__(self, property_type='houses', location='LE1,LE2,LE3,LE4,LE5,LE6,LE7,LE8,LE9,LE10,LE11,LE12,LE13,LE14,LE15,LE16,LE17,LE18,LE19,LE21,LE41,LE55,LE65,LE67,LE87,LE94,LE95,LE99', radius=0,property_sub_type='',beds_min='',beds_max='',price_max='',price_min='',page_size=100, **kwargs):
        urls=[]
        
        if ',' in location:
            location_list = location.split(",")
            for new_location in location_list:
                urls.append('https://www.zoopla.co.uk/to-rent/%s/%s/?q=%s%%&radius=%s%%&property_sub_type=%s%%&beds_min%s%%&beds_max=%s%%&price_max=%s%%&price_min=%s%%&page_size=%s' %(property_type,new_location,new_location,radius,property_sub_type,beds_min,beds_max,price_max,price_min,page_size))
        else:
            urls.append('https://www.zoopla.co.uk/to-rent/%s/%s/?q=%s%%&radius=%s%%&property_sub_type=%s%%&beds_min%s%%&beds_max=%s%%&price_max=%s%%&price_min=%s%%&page_size=%s' %(property_type,location,location,radius,property_sub_type,beds_min,beds_max,price_max,price_min,page_size))
            # print urls

        self.start_urls = urls
        super(ZooplaToRentSpider, self).__init__(**kwargs)

    def parse(self, response):
        houses=response.xpath("//div[contains(@id, 'content')]/ul/li")
        try:
            next_page = response.xpath("//div[contains(@class, 'paginate')]/a/text()")[-1].extract()
        except Exception as e:
            next_page=''


        for house in houses:
            item=Zoopla()
            description=house.xpath("div/div/a/@href").extract()
            monthly_prices=house.xpath("div/div/a/text()").extract()
            weekly_price=house.xpath("div/div/a/span/text()").extract()
            bedrooms=house.xpath("div/div/h3/span[contains(@class, 'num-beds')]/text()").extract()
            summaries=house.xpath("div/div/p[contains(@itemprop, 'description')]/text()").extract()
            addresses=house.xpath("div/div/span[contains(@itemprop, 'address')]/a/text()").extract()
            bathrooms=house.xpath("div/div/h3/span[contains(@class, 'num-baths')]/text()").extract()
            available_from=house.xpath("div/div/p[contains(@class, 'available-from')]/text()").extract()
            reception_rooms=house.xpath("div/div/h3/span[contains(@class, 'num-reception')]/text()").extract()
            agent_phone=house.xpath("div/div/div/p/span[contains(@class, 'agent_phone')]/a/span/text()").extract()
            station_distances=house.xpath("div/div/div[contains(@class, 'nearby_stations_schools')]/ul/li/text()").extract()
            agent=house.xpath("div/div/div/p[contains(@itemtype, 'https://schema.org/RealEstateAgent')]/span/text()").extract()
            stations=house.xpath("div/div/div[contains(@class, 'nearby_stations_schools')]/ul/li/span[contains(@class, 'nearby_stations_schools_name')]/text()").extract()

            formatted_beds=self.parse_string(bedrooms)
            formatted_agent=self.parse_string(agent)
            formatted_summary=self.parse_string(summaries)
            formatted_address=self.parse_string(addresses)
            formatted_bathrooms=self.parse_string(bathrooms)
            formatted_stations=self.parse_list_string(stations)
            formatted_agent_phone=self.parse_string(agent_phone)
            formatted_description=self.parse_string(description)
            formatted_weekly_price=self.parse_string(weekly_price)
            formatted_monthly_price=self.parse_string(monthly_prices)
            formatted_available_from=self.parse_string(available_from)
            formatted_reception_rooms=self.parse_string(reception_rooms)
            formatted_station_distance=self.parse_list_with_strip_string(station_distances)

            if formatted_available_from:
                if 'from' in formatted_available_from:
                    formatted_available_from=formatted_available_from.split('from')[1].strip()
                if 'immediately' in formatted_available_from:
                    formatted_available_from=datetime.now()
                elif formatted_available_from:
                    formatted_available_from=datetime.strptime(self.date_parser(formatted_available_from), '%d %b %Y')

            formatted_beds=int(formatted_beds) if formatted_beds else 0
            formatted_bathrooms=int(formatted_bathrooms) if formatted_bathrooms else 0
            try:
                formatted_monthly_price=int(re.sub("\D", "", formatted_monthly_price)) if formatted_monthly_price else 0
            except Exception as e:
                formatted_monthly_price=0
            try:
                formatted_weekly_price=int(re.sub("\D", "", formatted_weekly_price)) if formatted_monthly_price else 0
            except Exception as e:
                formatted_weekly_price=0
            formatted_reception_rooms=int(formatted_reception_rooms) if formatted_reception_rooms else 0

            if formatted_weekly_price:
                item['agent']=formatted_agent
                item['address']=formatted_address
                item['summary']=formatted_summary
                item['stations']=formatted_stations
                item['search_result_url']=response.url
                item['agent_number']=formatted_agent_phone
                item['weekly_price']=formatted_weekly_price
                item['monthly_price']=formatted_monthly_price
                item['available_from']=formatted_available_from
                item['station_distances']=formatted_station_distance
                item['bedrooms']=formatted_beds
                item['bathrooms']=formatted_bathrooms
                item['reception_rooms']=formatted_reception_rooms
                item['listing_url'] = 'https://www.zoopla.co.uk'+formatted_description

            if formatted_description:
                formatted_url='https://www.zoopla.co.uk'+formatted_description
                request = scrapy.Request(url=formatted_url, callback=self.parse_url)
                request.meta['item'] = item

            yield request

        try:
            if(next_page.encode('utf-8').strip()=='Next'):
                next_url=response.xpath("//div[contains(@class, 'paginate')]/a/@href")[-1].extract()
                next_url=next_url.encode('utf-8').strip() 
                formatted_url='https://www.zoopla.co.uk'+next_url
                next_url = response.urljoin(next_url)
                # print("next_url", next_url)
                yield scrapy.Request(next_url, callback=self.parse)
        except Exception as e:
            print('No next page')
      
    def date_parser(self, date):                                             
        return re.sub(r'(\d)(st|nd|rd|th)', r'\1', date)

    def parse_string(self, items):
        for item in items:
            return item.encode('utf-8').strip()            
            

    def parse_list_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item)
        return formatted_list

    def parse_list_with_strip_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item[1:-1])
        return formatted_list

    def concate_string(self,item_details):
        formatted_item_details=''

        for item_detail in item_details:
            formatted_item_detail=item_detail.encode('utf-8').strip()
            if formatted_item_detail:
                formatted_item_details=formatted_item_details+formatted_item_detail

        return formatted_item_details

    def parse_url(self, response):
        formatted_item_details=''
        item = response.meta['item']

        try:
            first_marketed_div = response.xpath('//div[contains(@id, "listings-agent")]/div')[3]
            first_marketed_p=first_marketed_div.xpath('p/text()').extract()[1]
        except Exception as e:
            first_marketed_p=''
        # postcode = response.xpath('//a[contains(@class, "tabs-right-link")]/@href').extract()[0]
        postcode=response.xpath('//meta[contains(@property, "og:postal-code")]/@content').extract()[0]
        description_details=response.xpath('//div[contains(@itemprop, "description")]//text()').extract()
        # local_authorities = response.xpath('//div[contains(@id, "content")]/div/div/div[contains(@id, "tab-local")]/div/ul/li/a/span/text()').extract()
        local_authorities = response.xpath('//h2[contains(text(), "Local info for")]/text()').extract()

        formatted_postcode=postcode.encode('utf-8').strip()
        formatted_description=self.concate_string(description_details)        
        # formatted_local_authority=self.parse_list_string(local_authorities)
        formatted_local_authority=self.concate_string(local_authorities)
        formatted_first_marketed=first_marketed_p.encode('utf-8').replace('\n', '')

        item['postcode']=formatted_postcode
        item['description'] = formatted_description
        # item['local_authority']=formatted_local_authority
        try:
            item['local_authority']=formatted_local_authority.split('for')[1].strip() if formatted_local_authority else ''
        except Exception as e:
            item['local_authority']=''
        try:
            formatted_first_marketed=formatted_first_marketed.split('on')[1].strip()
        except Exception as e:
            formatted_first_marketed=''
        item['first_marketed']=datetime.strptime(self.date_parser(formatted_first_marketed), '%d %b %Y') if formatted_first_marketed else ''

        yield item