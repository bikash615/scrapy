# -*- coding: utf-8 -*-
import scrapy
import re
from datetime import datetime
from decimal import Decimal
from freeman.code_decoder import data
from freeman.items import Rightmove
import requests
import json

class RightmoveToRentSpider(scrapy.Spider):
    name = 'rightmove_to_rent'
    allowed_domains = ['rightmove.co.uk']
    # start_urls = ['http://www.rightmove.co.uk/property-for-sale/find.html?searchType=SALE&locationIdentifier=OUTCODE%5E750&insId=9&radius=0.0&minPrice=&maxPrice=&minBedrooms=&maxBedrooms=&displayPropertyType=&maxDaysSinceAdded=&_includeSSTC=on&sortByPriceDescending=&primaryDisplayPropertyType=&secondaryDisplayPropertyType=&oldDisplayPropertyType=&oldPrimaryDisplayPropertyType=&newHome=&auction=false']

    def __init__(self, searchType='sale', location='LE1,LE2,LE3,LE4,LE5,LE6,LE7,LE8,LE9,LE10,LE11,LE12,LE13,LE14,LE15,LE16,LE17,LE18,LE19,LE21,LE41,LE55,LE65,LE67,LE87,LE94,LE95,LE99',radius='0.0', ins_id='1',display_property_type='',beds_min='',beds_max='',price_max='',price_min='',max_days_since_added='',_include_SSTC='on', **kwargs):
        urls=[]

        if ',' in location:
            location_list = location.split(",")
            for new_location in location_list:
                decoded_location=self.decode_location(new_location)
                urls.append('https://www.rightmove.co.uk/property-to-rent/find.html?searchType=%s%%&insId=%s%%&radius=%s%%&displayPropertyType=%s%%&minBedrooms%s%%&maxBedrooms=%s%%&maxPrice=%s%%&minPrice=%s%%&maxDaysSinceAdded=%s%%&_includeSSTC=%s%%&locationIdentifier=OUTCODE%%5E%s' %(searchType,ins_id,radius,display_property_type,beds_min,beds_max,price_max,price_min,max_days_since_added,_include_SSTC,decoded_location))
        else:
            decoded_location=self.decode_location(location)
            urls.append('https://www.rightmove.co.uk/property-to-rent/find.html?searchType=%s%%&insId=%s%%&radius=%s%%&displayPropertyType=%s%%&minBedrooms%s%%&maxBedrooms=%s%%&maxPrice=%s%%&minPrice=%s%%&maxDaysSinceAdded=%s%%&_includeSSTC=%s%%&locationIdentifier=OUTCODE%%5E%s' %(searchType,ins_id,radius,display_property_type,beds_min,beds_max,price_max,price_min,max_days_since_added,_include_SSTC,decoded_location))

        self.start_urls = urls
        super(RightmoveToRentSpider, self).__init__(**kwargs)

    def parse(self, response):
        properties=response.xpath("//div[contains(@id, 'l-searchResults')]/div[contains(@class, 'is-list')]")
        total_number_of_data = int(response.xpath('//span[contains(@class, "searchHeader-resultCount")]/text()').extract()[0].replace(',', ''))
        search_result_url = response.url
        locationIdentifier=search_result_url.split('locationIdentifier=')[1]        
        listing_url = response.url.split('?')[0] if len(response.url.split('?')) else response.url
        for _property in properties:
            request = self.parse_from_html(_property, search_result_url, listing_url)
            yield request

        for index in range(1, (total_number_of_data / 24 + 1)):
            json_url = 'http://www.rightmove.co.uk/api/_search?locationIdentifier='+locationIdentifier+'&numberOfPropertiesPerPage=24&radius=0.0&sortType=6&index=%s&viewType=LIST&channel=RENT&areaSizeUnit=sqft&currencyCode=GBP&isFetching=false&viewport=' % str(index * 24)
            res = requests.get(
                json_url,
                headers={'User-Agent': 'test'},
                stream=True
            )
            res.raw.decode_content = True
            res = res.raw.data.decode("utf-8")
            output = json.loads(res)
            __properties = output.get('properties')
            for each_property in __properties:
                request = self.parse_json_api(
                    each_property,
                    search_result_url,
                    listing_url
                )
                yield request

    def parse_json_api(self, each_property, search_result_url, listing_url):
        if (each_property.get('transactionType').encode('utf-8').strip()!='rent'):
            return;
        item = Rightmove()
        # item['price'] = each_property.get('price').get('amount')
        base_url = 'https://www.rightmove.co.uk'

        monthly_price=each_property.get('price').get("displayPrices")[0].get("displayPrice").encode('utf-8').strip()
        weekly_price=each_property.get('price').get("displayPrices")[1].get("displayPrice").encode('utf-8').strip()

        item['agent'] = each_property.get('customer').get('branchDisplayName').encode('utf-8').strip()
        item['summary'] = each_property.get('summary').encode('utf-8').strip()

        try:
            item['monthly_price'] = int(re.sub("\D", "", monthly_price)) if monthly_price else 0
        except Exception as e:
            item['monthly_price']=0

        try:
            item['weekly_price'] = int(re.sub("\D", "", weekly_price)) if weekly_price else 0
        except Exception as e:
            item['weekly_price']=0

        item['address'] = each_property.get('displayAddress').encode('utf-8').strip()
        item['search_result_url'] = search_result_url
        item['agent_number'] = each_property.get('contactTelephone').encode('utf-8').strip() if each_property.get('contactTelephone') else None
        item['listing_url'] = base_url + each_property.get('propertyUrl').encode('utf-8').strip()
        formatted_url = base_url + each_property.get('propertyUrl').encode('utf-8').strip()
        request = scrapy.Request(
            url=formatted_url,
            callback=self.parse_url
        )
        request.meta['item'] = item
        return request

    def parse_from_html(self, property, search_result_url, listing_url):
        item=Rightmove()

        # price=property.xpath("div/div[contains(@class, 'propertyCard-header')]/div/a/div/text()").extract()
        detail_link=property.xpath("div/div[contains(@class, 'propertyCard-header')]/div/a/@href").extract()
        monthly_price=property.xpath('div/div[contains(@class, "propertyCard-header")]/div[contains(@class, "propertyCard-price")]/a/div/span[contains(@class, "propertyCard-priceValue")]/text()').extract()
        weekly_price=property.xpath('div/div[contains(@class, "propertyCard-header")]/div[contains(@class, "propertyCard-price")]/div[contains(@class, "propertyCard-rentalPrice-secondary")]/a/span[contains(@class, "propertyCard-secondaryPriceValue")]/text()').extract()
        agent_number=property.xpath("div/div[contains(@class, 'propertyCard-contacts')]/div/p[contains(@class, 'propertyCard-contactsItemDetails')]/a[contains(@class, 'propertyCard-contactsPhoneNumber')]/text()").extract()
        summary=property.xpath("div/div[contains(@class, 'propertyCard-content')]/div[contains(@class, 'propertyCard-section')]/div[contains(@class, 'propertyCard-description')]/a/span/text()").extract()
        addresses=property.xpath("div/div[contains(@class, 'propertyCard-content')]/div[contains(@class, 'propertyCard-section')]/div[contains(@class, 'propertyCard-details')]/a/address/span/text()").extract()
        agent=property.xpath("div/div[contains(@class, 'propertyCard-content')]/div[contains(@class, 'propertyCard-detailsFooter')]/div[contains(@class, 'propertyCard-branchSummary')]/span[contains(@class, 'propertyCard-branchSummary-branchName')]/text()").extract()

        # formatted_price=self.parse_string(price)
        formatted_agent=self.parse_string(agent)
        formatted_summary=self.parse_string(summary)
        formatted_address=self.parse_string(addresses)
        formatted_detail_link=self.parse_string(detail_link)
        formatted_agent_number=self.parse_string(agent_number)
        formatted_weekly_price=self.parse_string(weekly_price)
        formatted_monthly_price=self.parse_string(monthly_price)

        try:
            formatted_monthly_price=int(re.sub("\D", "", formatted_monthly_price)) if formatted_monthly_price else 0
        except Exception as e:
            formatted_monthly_price=0
        try:
            formatted_weekly_price=int(re.sub("\D", "", formatted_weekly_price)) if formatted_weekly_price else 0
        except Exception as e:
            formatted_weekly_price=0

        if formatted_agent:
            formatted_agent=formatted_agent.replace('by ', '')

        if formatted_address:
            # item['price']=formatted_price
            item['agent']=formatted_agent
            item['summary']=formatted_summary
            item['address']=formatted_address
            item['search_result_url']=search_result_url
            item['weekly_price']=formatted_weekly_price
            item['agent_number']=formatted_agent_number
            item['monthly_price']=formatted_monthly_price
            item['listing_url'] = 'https://www.rightmove.co.uk'+formatted_detail_link
                       
        if formatted_address:
            formatted_url='https://www.rightmove.co.uk'+formatted_detail_link
            request = scrapy.Request(url=formatted_url, callback=self.parse_url)
            request.meta['item'] = item
        
            return request
        
    def parse_string(self, items):
        for item in items:
            return item.encode('utf-8').strip()

    def decode_location(self, location):
        decode_location=''
        for item in data:
            if item['outcode'].lower()==location.lower():
                decode_location= str(item['code'])
                break
        return decode_location               

    def parse_list_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item)
        return formatted_list

    def parse_list_with_strip_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item[1:-1])
        return formatted_list

    def concate_string(self,item_details):
        formatted_item_details=''

        for item_detail in item_details:
            formatted_item_detail=item_detail.encode('utf-8').strip()
            if formatted_item_detail:
                formatted_item_details=formatted_item_details+formatted_item_detail

        return formatted_item_details

    def parse_url(self, response):
        first_marketed=''
        filter_postcode=''
        formatted_first_marketed=''
        item = response.meta['item']

        postcodes=response.xpath('//script').extract()
        position=response.xpath('//div[contains(@class, "pos-rel")]/a/img/@src').extract()
        letting_informations=response.xpath('//div[contains(@id, "lettingInformation")]/table/tbody/tr')
        stations = response.xpath('//div[contains(@class, "nearest-stations")]/div/ul/li/span/text()').extract()
        bedrooms=response.xpath('//div[contains(@class, "property-header-bedroom-and-price")]/div/h1/text()').extract()
        station_distances = response.xpath('//div[contains(@class, "nearest-stations")]/div/ul/li/small/text()').extract()
        descriptions=response.xpath('//div[contains(@id, "description")]/div/div[contains(@class,"agent-content")]/div[contains(@class, "sect")]/p/text()').extract()       

        for postcode in postcodes:
            if 'propertyPostcode' in postcode:
                filter_postcode = postcode.split("propertyPostcode:")[-1].split(',')[0]
            if 'added' in postcode:
                if 'RIGHTMOVE.ANALYTICS.DataLayer.pushKV' in postcode:
                    keys = postcode.split("added:")[-1].split(',')
                    for key in keys:
                        if("added" in key):
                            first_marketed= key.split("added")[1].replace(':','').replace('"', '')


        formatted_postcode=str(filter_postcode)
        formatted_beds_string=self.parse_string(bedrooms)
        formatted_position=self.parse_string(position)
        formatted_first_marketed=str(first_marketed)
        formatted_station=self.parse_list_string(stations)
        formatted_description=self.concate_string(descriptions)
        formatted_latitude=formatted_position.split('latitude=')[-1].split('&')[0]
        formatted_longitude = formatted_position.split('longitude=')[-1].split('&')[0]
        formatted_station_distance=self.parse_list_with_strip_string(station_distances)
        formatted_bed=formatted_beds_string.split('bedroom')[0] if formatted_beds_string else 'N/A'

        first_marketed=''
        formatted_furnishing=''
        formatted_letting_type=''
        formatted_date_available=''

        for information in letting_informations:
            types=information.xpath('td/text()').extract()[0]
            encoded_types=types.encode('utf-8')

            if(encoded_types=='Date available:'):
                formatted_date_available=information.xpath('td/text()').extract()[1]
            elif(encoded_types=='Furnishing:'):
                formatted_furnishing=information.xpath('td/text()').extract()[1]
            elif(encoded_types=='Letting type:'):
                formatted_letting_type=information.xpath('td/text()').extract()[1]

        formatted_latitude=Decimal(formatted_latitude) if formatted_latitude else 0
        formatted_longitude=Decimal(formatted_longitude) if formatted_longitude else 0

        item['bedrooms']=formatted_bed.strip()
        item['stations'] = formatted_station
        item['latitude']=formatted_latitude
        item['longitude']=formatted_longitude
        item['description']=formatted_description
        item['first_marketed']=datetime.strptime(formatted_first_marketed, '%Y%m%d') if formatted_first_marketed else datetime.now()
        item['station_distances']=formatted_station_distance
        item['postcode']=formatted_postcode.strip().replace('"', '')
        item['furnishing']=formatted_furnishing.encode('utf-8') if formatted_furnishing else ''
        item['letting_type']=formatted_letting_type.encode('utf-8') if formatted_letting_type else ''

        formatted_date_available=formatted_date_available.encode('utf-8') if formatted_description else ''
        item['bedrooms']=int(item['bedrooms']) if item['bedrooms'].isdigit() else 0

        if formatted_date_available:
            if 'Now' in formatted_date_available:
                formatted_date_available=formatted_date_available=datetime.now()
            elif formatted_date_available:
                formatted_date_available=datetime.strptime(formatted_date_available, '%d/%m/%Y')
        else:
            formatted_date_available=None

        item['available_from']=formatted_date_available

        yield item