# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from decimal import Decimal
from datetime import datetime
from freeman.items import Zoopla

class ZooplaSpider(scrapy.Spider):
    name = 'zoopla_for_sale'
    allowed_domains = ['zoopla.co.uk']
    # rules = (Rule(LinkExtractor(allow=(), restrict_xpaths=('//div[@class="paginate"]',)), callback="parse_page", follow= True),)
    # start_urls = ['https://www.zoopla.co.uk/to-rent/houses/e15/?q=e15&radius=0&property_sub_type=&beds_min=&beds_max=&price_max=&price_min=&page_size=100']
    def __init__(self, property_type='houses', location='e14,e15', radius=0,property_sub_type='',beds_min='',beds_max='',price_max='',price_min='',page_size=25, **kwargs):
    # def __init__(self, property_type='houses', location='LE1,LE2,LE3,LE4,LE5,LE6,LE7,LE8,LE9,LE10,LE11,LE12,LE13,LE14,LE15,LE16,LE17,LE18,LE19,LE21,LE41,LE55,LE65,LE67,LE87,LE94,LE95,LE99', radius=0,property_sub_type='',beds_min='',beds_max='',price_max='',price_min='',page_size=25, **kwargs):
        urls=[]
        
        if ',' in location:
            location_list = location.split(",")
            for new_location in location_list:
                urls.append('https://www.zoopla.co.uk/for-sale/%s/%s/?q=%s%%&radius=%s%%&property_sub_type=%s%%&beds_min%s%%&beds_max=%s%%&price_max=%s%%&price_min=%s%%&page_size=%s' %(property_type,new_location,new_location,radius,property_sub_type,beds_min,beds_max,price_max,price_min,page_size))
        else:
            urls.append('https://www.zoopla.co.uk/for-sale/%s/%s/?q=%s%%&radius=%s%%&property_sub_type=%s%%&beds_min%s%%&beds_max=%s%%&price_max=%s%%&price_min=%s%%&page_size=%s' %(property_type,location,location,radius,property_sub_type,beds_min,beds_max,price_max,price_min,page_size))

        # Rules = (Rule(LinkExtractor(allow=(), restrict_xpaths=('//div[@class="paginate"]/a[contains(text(),"Next")',)), callback="parse", follow= True),) 

        self.start_urls = urls
        super(ZooplaSpider, self).__init__(**kwargs)

    def parse(self, response):
        houses=response.xpath("//div[contains(@id, 'content')]/ul/li")
        try:
            next_page = response.xpath("//div[contains(@class, 'paginate')]/a/text()")[-1].extract()
        except Exception as e:
            next_page=''

        for house in houses:
            item=Zoopla()
            prices=house.xpath("div/div/a/text()").extract()
            description=house.xpath("div/div/a/@href").extract()
            bedrooms=house.xpath("div/div/h3/span[contains(@class, 'num-beds')]/text()").extract()
            summaries=house.xpath("div/div/p[contains(@itemprop, 'description')]/text()").extract()
            addresses=house.xpath("div/div/span[contains(@itemprop, 'address')]/a/text()").extract()
            bathrooms=house.xpath("div/div/h3/span[contains(@class, 'num-baths')]/text()").extract()
            reception_rooms=house.xpath("div/div/h3/span[contains(@class, 'num-reception')]/text()").extract()
            agent_phone=house.xpath("div/div/div/p/span[contains(@class, 'agent_phone')]/a/span/text()").extract()
            station_distances=house.xpath("div/div/div[contains(@class, 'nearby_stations_schools')]/ul/li/text()").extract()
            agent=house.xpath("div/div/div/p[contains(@itemtype, 'https://schema.org/RealEstateAgent')]/span/text()").extract()
            latitude=house.xpath("div/div/div[contains(@itemprop, 'geo')]/meta[contains(@itemprop, 'latitude')]/@content").extract()
            longitude=house.xpath("div/div/div[contains(@itemprop, 'geo')]/meta[contains(@itemprop, 'longitude')]/@content").extract()
            stations=house.xpath("div/div/div[contains(@class, 'nearby_stations_schools')]/ul/li/span[contains(@class, 'nearby_stations_schools_name')]/text()").extract()

            formatted_beds=self.parse_string(bedrooms)
            formatted_price=self.parse_string(prices)
            formatted_agent=self.parse_string(agent)
            formatted_summary=self.parse_string(summaries)
            formatted_address=self.parse_string(addresses)
            formatted_latitude=self.parse_string(latitude)
            formatted_longitude=self.parse_string(longitude)
            formatted_bathrooms=self.parse_string(bathrooms)
            formatted_stations=self.parse_list_string(stations)
            formatted_agent_phone=self.parse_string(agent_phone)
            formatted_description=self.parse_string(description)
            formatted_reception_rooms=self.parse_string(reception_rooms)
            formatted_station_distance=self.parse_list_with_strip_string(station_distances)

            formatted_beds=int(formatted_beds) if formatted_beds else 0
            formatted_bathrooms=int(formatted_bathrooms) if formatted_bathrooms else 0
            try:
                formatted_price=int(re.sub("\D", "", formatted_price)) if formatted_price else 0
            except Exception as e:
                formatted_price=0
            formatted_latitude=Decimal(formatted_latitude) if formatted_latitude else 0
            formatted_longitude=Decimal(formatted_longitude) if formatted_longitude else 0
            formatted_reception_rooms=int(formatted_reception_rooms) if formatted_reception_rooms else 0

            if formatted_price:
                item['price']=formatted_price
                item['agent']=formatted_agent
                item['address']=formatted_address
                item['summary']=formatted_summary
                item['stations']=formatted_stations
                item['latitude']=formatted_latitude
                item['longitude']=formatted_longitude
                item['search_result_url']=response.url
                item['agent_number']=formatted_agent_phone
                item['station_distances']=formatted_station_distance
                item['bedrooms']=formatted_beds
                item['bathrooms']=formatted_bathrooms
                item['reception_rooms']=formatted_reception_rooms
                item['listing_url'] = 'https://www.zoopla.co.uk'+formatted_description
            
            if formatted_description:
                formatted_url='https://www.zoopla.co.uk'+formatted_description
                request = scrapy.Request(url=formatted_url, callback=self.parse_url)
                request.meta['item'] = item

            yield request

        try:
            if(next_page.encode('utf-8').strip()=='Next'):
                next_url=response.xpath("//div[contains(@class, 'paginate')]/a/@href")[-1].extract()
                next_url=next_url.encode('utf-8').strip() 
                formatted_url='https://www.zoopla.co.uk'+next_url
                next_url = response.urljoin(next_url)
                # print("next_url", next_url)
                yield scrapy.Request(next_url, callback=self.parse)
        except Exception as e:
            print('No next page')
        
    def parse_string(self, items):
        for item in items:
            return item.encode('utf-8').strip()            

    def parse_list_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item)
        return formatted_list

    def parse_list_with_strip_string(self,items):
        formatted_list=[]
        for item in items:
            formatted_item=item.encode('utf-8').strip()
            if formatted_item:
                formatted_list.append(formatted_item[1:-1])
        return formatted_list

    def concate_string(self,item_details):
        formatted_item_details=''

        for item_detail in item_details:
            formatted_item_detail=item_detail.encode('utf-8').strip()
            if formatted_item_detail:
                formatted_item_details=formatted_item_details+formatted_item_detail

        return formatted_item_details

    def date_parser(self, date):                                             
        return re.sub(r'(\d)(st|nd|rd|th)', r'\1', date)

    def parse_url(self, response):
        formatted_item_details=''
        scripts=response.xpath('//script').extract()
        item = response.meta['item']

        listing_history=response.xpath('//div[contains(@class, "sidebar sbt")]/p[contains(@class, "top")]')
        updated_price_list=response.xpath('//div[contains(@class, "sidebar sbt")]/div[contains(@class, "top")]/ul/li')
        
        # listing_history=response.xpath('//h4[contains(@text, "Listing history")]/ancestor::div[contains(@class, "sidebar sbt")]').extract()
        # listing_history=response.xpath('//div[@class="sidebar sbt" and contains(.//h4, "Listing history")]')
        first_marketed_div = response.xpath('//div[contains(@id, "listings-agent")]/div')[3]
        first_marketed_p=first_marketed_div.xpath('p/text()').extract()[1]
        postcode=response.xpath('//meta[contains(@property, "og:postal-code")]/@content').extract()[0]
        # postcode = response.xpath('//a[contains(@class, "tabs-right-link")]/@href').extract()[0]
        description_details=response.xpath('//div[contains(@class, "bottom-plus-half")]/div[contains(@class, "top")]//text()').extract()
        # local_authorities = response.xpath('//div[contains(@id, "content")]/div/div/div[contains(@id, "tab-local")]/div/ul/li/a/span/text()').extract()
        local_authorities = response.xpath('//h2[contains(text(), "Local info for")]/text()').extract()

        tenure=''
        property_type=''
        last_listed_date=''
        first_listed_date=''
        last_listed_price=0
        first_listed_price=0
        for history in listing_history:
            topic=history.xpath('strong/text()').extract()
            topic=topic[0].decode('utf-8').strip() if len(topic)>0 else ''

            if(topic=='First listed'):
                first_listed=history.xpath('text()').extract()[1]
                listed_price=first_listed.encode('utf-8').split('on')[0].strip()
                try:
                    listed_date=first_listed.encode('utf-8').split('on')[1].strip()
                except Exception as e:
                    listed_date=''
                first_listed_price=int(re.sub("\D", "", listed_price)) if listed_price else 0
                first_listed_date=datetime.strptime(self.date_parser(listed_date), '%d %b %Y') if listed_date else ''

            if(topic=='Last sale'):
                last_listed=history.xpath('text()').extract()[1]
                last_price=last_listed.encode('utf-8').split('on')[0].strip()
                last_date=last_listed.encode('utf-8').split('on')[1].strip()
                last_listed_price=int(re.sub("\D", "", last_price)) if last_price else 0
                last_listed_date=datetime.strptime(self.date_parser(last_date), '%d %b %Y') if last_date else ''

        for script in scripts:
            if 'customVariables' in script:
                if 'property_type' in script:
                    property_type = script.split('property_type":')[-1].split(',')[0]
                if 'tenure' in script:
                    tenure=script.split('"tenure":')[-1].split(',')[0]

        updated_price_rate_list=updated_price_list.xpath('strong/text()').extract()
        updated_price_date_list=updated_price_list.xpath('span[contains(@class, "date")]/text()').extract()

        if updated_price_date_list:
            updated_price_rate=updated_price_rate_list[-1].encode('utf-8').strip()
            updated_price_date_list=updated_price_date_list[-1].encode('utf-8').split('on:')[1].strip()
            price_last_updated=datetime.strptime(self.date_parser(updated_price_date_list), '%d %b %Y') if updated_price_date_list else ''
        else:
            price_last_updated=''
            updated_price_rate=''

        formatted_postcode=postcode.encode('utf-8').strip()
        formatted_description=self.concate_string(description_details)        
        formatted_local_authority=self.concate_string(local_authorities)
        try:
            formatted_first_marketed=first_marketed_p.encode('utf-8').replace('\n', '')
        except Exception as e:
            formatted_first_marketed=''

        item['last_sale_date']=last_listed_date
        item['price_adjusted']=updated_price_rate
        item['last_sale_price']=last_listed_price
        item['first_listed_date']=first_listed_date
        item['description'] = formatted_description
        item['price_last_updated']=price_last_updated
        item['first_listed_price']=first_listed_price
        item['property_type']=property_type.strip('"').encode('utf-8').strip() if property_type else ''
        item['tenure']=tenure.strip('"').encode('utf-8').strip() if tenure else ''
        # item['postcode']=formatted_postcode.split('postcode=')[1] if formatted_postcode else ''
        item['postcode']=formatted_postcode
        try:
            formatted_first_marketed=formatted_first_marketed.split('on')[1].strip() if formatted_first_marketed else ''
        except Exception as e:
            formatted_first_marketed=''
        item['local_authority']=formatted_local_authority.split('for')[1].strip() if formatted_local_authority else ''
        item['first_marketed']=datetime.strptime(self.date_parser(formatted_first_marketed), '%d %b %Y') if formatted_first_marketed else ''

        yield item
