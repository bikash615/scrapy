# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Zoopla(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    bedrooms=scrapy.Field()
    agent=scrapy.Field()
    price = scrapy.Field()
    stations=scrapy.Field()
    postcode=scrapy.Field()
    latitude=scrapy.Field()
    address = scrapy.Field()
    summary = scrapy.Field()
    longitude=scrapy.Field()
    description=scrapy.Field()
    bathrooms = scrapy.Field()
    listing_url=scrapy.Field()
    agent_number=scrapy.Field()
    weekly_price=scrapy.Field()
    monthly_price=scrapy.Field()
    first_marketed=scrapy.Field()
    last_sale_date=scrapy.Field()
    available_from=scrapy.Field()
    last_sale_price=scrapy.Field()
    local_authority=scrapy.Field()
    reception_rooms=scrapy.Field()
    price_adjusted=scrapy.Field()
    property_type=scrapy.Field()
    tenure=scrapy.Field()
    first_listed_date=scrapy.Field()
    price_last_updated=scrapy.Field()
    search_result_url=scrapy.Field()
    station_distances=scrapy.Field()
    first_listed_price=scrapy.Field()

class Rightmove(scrapy.Item):
    bedrooms=scrapy.Field()
    price=scrapy.Field()
    agent=scrapy.Field()
    stations=scrapy.Field()
    address=scrapy.Field()
    summary=scrapy.Field()
    postcode=scrapy.Field()
    latitude=scrapy.Field()
    longitude=scrapy.Field()
    furnishing=scrapy.Field()
    description=scrapy.Field()
    listing_url=scrapy.Field()
    weekly_price=scrapy.Field()
    letting_type=scrapy.Field()
    agent_number=scrapy.Field()
    monthly_price=scrapy.Field()
    available_from=scrapy.Field()
    first_marketed=scrapy.Field()
    station_distances=scrapy.Field()
    search_result_url=scrapy.Field()

