# -*- coding: utf-8 -*-
import scrapy
from freeman.items import FreemanItem


class ZooplaSpider(scrapy.Spider):
    name = 'zoopla'
    allowed_domains = ['zoopla.co.uk']
    start_urls = ['https://www.zoopla.co.uk/for-sale/houses/e15/?q=e15&radius=0&property_sub_type=&beds_min&beds_max=&price_max=&price_min=']

    def parse(self, response):
        item=FreemanItem()

        houses=response.xpath("//div[contains(@id, 'content')]/ul/li")
        print len(houses)
        c=0
        for house in houses:
            prices=house.xpath("div/div/a/text()").extract()
            addresses=house.xpath("div/div/div/p/span/text()").extract()
            
            c=c+1
            formatted_price=self.parse_string(prices)
            formatted_address=self.parse_string(addresses)
            if formatted_price:
                item['price']=formatted_price
                item['address']=formatted_address
            yield item
        
    def parse_string(self, items):
        for item in items:
            return item.encode('utf-8').strip()            
            
